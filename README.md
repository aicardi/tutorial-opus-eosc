# Tutorial OPUS EOSC

This tutorial aims to create a simple but full environment on a server to
illustrate OPUS deployment and configuration on a cloud service.  The provided
scripts try to automate the deployment with a basic configuration. They are not
meant to provide a production service.  All scripts were tested on EOSC servers
with a Debian 11 OS.

We propose three types of installations: 
- a standalone server
- a standalone server with a docker container
- a server together with a minimal HPC cluster

The latter is temporarly available [here](https://voparis-opus-eosc.obspm.fr/opus_client).


## Deploy and test a standalone OPUS server

```shell
sudo apt update
sudo apt install -y git
git clone https://gitlab.obspm.fr/aicardi/tutorial-opus-eosc.git
cd tutorial-opus-eosc
./deploy-bare-metal.sh
```

## Deploy and test OPUS in docker

```shell
sudo apt update
sudo apt install -y git
git clone https://gitlab.obspm.fr/aicardi/tutorial-opus-eosc.git
cd tutorial-opus-eosc
./deploy-docker.sh
```

## Deploy OPUS with a slurm cluster in EOSC

### Architecture

We will create a OPUS server in our own domain (here obspm.fr) and offload
calculations to a slurm cluster in EOSC.

```mermaid
%%{ init: { 'flowchart': { 'curve': 'step' } } }%%
flowchart LR
  Client([Client]) --> OPUS
  OPUS --> Master

  subgraph EOSC["<img src='https://eosc.eu/themes/paranoid/logo-eosc.png' width='150px'/>\nA big transparent text"]
  Master(Slurm Frontal) --- dummy[ ]
  dummy --> Node1[Node 1]
  dummy ---  Nodeetc[...]
  dummy --> Noden[Node n]

  linkStyle 4 display:none;
  style dummy display:none;
  style EOSC fill:#ddd,color:#ddd;

end
```

This tutorial assumes that you have enough rights to create and destroy virtual machines in EOSC.

You need to authorize SSH traffic from EOSC to your OPUS server in your firewall.

### Create network and servers

See [the doc to interact with
EOSC](https://voparis-wiki.obspm.fr/display/VES/Create+a+openstack+ubuntu+machine+on+EOSC+with+ports+80+and+8080+open).

We briefly present what is needed for the installation in our case

First get an access token and put it in the environment variable `ACCESS_TOKEN`
```shell
pip install fedcloudclient
SITE='CESNET-MCC'
PROJECT=`fedcloud endpoint projects --site $SITE --oidc-access-token=$ACCESS_TOKEN | grep vespa | cut -f1 -d\ `
source <(fedcloud endpoint env --site $SITE --oidc-access-token=$ACCESS_TOKEN --project-id $PROJECT)

export FLAVOR=standard.medium
export IMAGE=debian-11-x86_64

openstack security group create vespacluster --description "opening port 6817 and 6818 for a slurm cluster"
openstack security group rule create vespacluster --protocol tcp --dst-port 6817:6817 --remote-ip 192.168.11.0/24
openstack security group rule create vespacluster --protocol tcp --dst-port 6818:6818 --remote-ip 192.168.11.0/24

openstack server create --flavor $FLAVOR --image $IMAGE --nic net-id=mynetwork --security-group vespagroup --security-group vespacluster --key-name my-key1 frontal
openstack server create --flavor $FLAVOR --image $IMAGE --nic net-id=mynetwork --security-group vespagroup --security-group vespacluster --key-name my-key1 node1
...
openstack server create --flavor $FLAVOR --image $IMAGE --nic net-id=mynetwork --security-group vespagroup --security-group vespacluster --key-name my-key1 noden

openstack server list
# note the ip adresses, we will need them later and wait until all servers are UP
```

### Deploy OPUS server

Connect to the VM with the command `ssh -A IP_OPUS_SERVER`. The `-A`
option is important to be able to connect to all nodes from the OPUS server.

```shell
sudo apt update
sudo apt install -y git
git clone https://gitlab.obspm.fr/aicardi/tutorial-opus-eosc.git
cd tutorial-opus-eosc
cp parameters.sh.tmpl parameters.sh
# edit the file parameters.sh and put the right node names and IP addresses
./deploy-opus-with-slurm.sh
```

The previous command prepares installation on all nodes. In particular, all
secrets keys needed are created on this server and sent to the others.

### Deploy cluster

Connect to the frontal node :
```shell
cd tutorial-opus-eosc
./deploy-slurm-nodes.sh master
```

You can check that the node is working with this command
```shell
debian@frontal:~/tutorial-opus-eosc$ sinfo 
PARTITION AVAIL  TIMELIMIT  NODES  STATE NODELIST
opus*        up   infinite      2   unk* node[1-2]
```
If it doesn't work, try to restart the slurm controller daemon.
```shell
sudo systemctl restart slurmctld
```

Now connect on each cluster node. 

```shell
cd tutorial-opus-eosc
./deploy-slurm-nodes.sh
```

You can now submit a sample job via the OPUS webpage, see the job submitted in
slurm and executed on one of the nodes.


## Remarks

### Production deployment
For a production deployment, you need a real domain name for the server, configure apache with a SSL certificate
and change most of the parameters provided in the `settings_opus.py` file (namely tokens, passwords, emails...)

We didn't implement the connection with Eduteams login here, but OPUS can
handle it. First declare the service in Eduteams. You will get an ID and a
SECRET. Than change the `settings-opus.py` file like this.
```python
OIDC_IDPS = [
    {
        "title": "eduTeams",
        "description": "login through eduTeams",
        "url": "https://proxy.eduteams.org/.well-known/openid-configuration",
        "url_logo": "https://eduteams.org/img/favicon.png",
        "client_id": "ID_FROM_EDUTEAMS",
        "client_secret": "SECRET_FROM_EDUTEAMS",
        "scope": "openid email profile orcid eduperson_principal_name",
    },
]
```

### Slurm cluster deployment

The scripts provided here create a minimal cluster with two nodes. We do not
create a NFS server here since its configuration would require much more work
to allow the right network permissions. 

On the other hand slurm relies heavily on shared disks. Since OPUS uses only
one user, we chose to share data via sshfs with the right options. This
solution doesn't need any network configuration as the ssh port is obviously
open on all servers. We didn't try this on a large cluster, there could be
problems scaling up.

### Elasticity

To optimize resource use, we use the power save support for idle nodes in slurm.
For this, we need to interact with openstack.

First we need a refresh token: connect to the [EGI Token Portal](https://aai.egi.eu/token/), click on the "Create Refresh Token" button and copy its contents to the file `/usr/local/etc/refresh_token` on the frontal node.
then
```shell
sudo chown root.root /usr/local/etc/refresh_token
sudo chmod 400 /usr/local/etc/refresh_token
```


