#!/usr/bin/python3
# -*- coding: iso-8859-15 -*-

import re
import datetime
import sys
import json
import os
os.environ["XDG_CONFIG_HOME"]="/tmp"
os.environ["XDG_CACHE"]="/tmp/astropy"
from astropy.io.votable.tree import VOTableFile, Resource, Table, Field, Param
from astropy.time import Time
from astropy.io import fits
import numpy as np
import cgi
import urllib
import pandas as pd
import matplotlib.pyplot as plt

# Write VOtables from ascii spectra found on-line
# input is a CSV file of EPNCore metadata 
# only save the thumbanils if spectra are already provided as votable - nothing if fits


#########  INITIAILISATION
fichier_json = "/usr/local/share/opus-sample/m4ast.json"


# formatting information to make the votable : list of field/param, UCD, unit in the json file
file = open(fichier_json)
jr = json.load(file)
#print jr


def pl_thumb(metaD):
    '''
    draw thumbnails independently
    '''

    Nfichier = metaD["access_url"].split('/')[-1].split('.')[0]
    fig = plt.figure(figsize=(2.2, 2.2), dpi=80)
    ax = fig.add_subplot(111)
    plt.locator_params(nbins=4)
    # wave & reflect must be floats (done)
    #print((table.fields))
    wave= (table.array["Wavelength"])
    #print([table.array][0])
    #print((table.fields[1]).name)
    N2 = (table.fields[1]).name
    reflect= (table.array[N2])
    ax.plot(wave, reflect, 'r')
    plt.xlabel("wavelength (um)", fontsize=6, color='b')
    plt.xticks(size=6)
    plt.yticks(size=6)
    ax.set_ylabel("level", fontsize = 8)
    plt.tick_params(labelsize=8)
    plt.rc('font', size=8)
    ax.text(.5, .85, Nfichier, horizontalalignment='center', transform=ax.transAxes)
    plt.tight_layout()
    plt.savefig(Nfichier+'.png' )
    plt.close()



#  uses Astropy to make the votable, check the version of astropy
# http://ivoa.net/documents/SpectralDM/20150528/PR-SpectralDM-2.0-20150528.pdf

def to_votable(metaD):
    '''
    create a votable of a spectrum from information read in a row of an EPNCore table 
    due to non uniform format of text file, it checks all metadata
    :param filename: name of txt file
    :return:
    '''

    wave=[]
    reflect=[]
    error=[]
    presence_error = 0
    filename = metaD["access_url"]

# grab data on VizieR, more stable than JAXA
    Ftype = metaD["access_format"]

# get ascii data from VizieR if script provided - ok but has to handle emissivity
    if Ftype != 'text/plain':
        return 'not_ascii'

    # open provided access_url
    #print(filename)
    fichier = urllib.request.urlopen(filename)
    montab = fichier.readlines()
    charset = fichier.info().get_content_charset()
    fichier.close()
    if not charset:
        charset = 'utf-8'
    # **************** read Header
    #i=0
    flag=0
    #print(metaD["target_name"])

    for line in montab:
        if line.decode(charset).strip() == "":
            continue
        ligne = line.decode(charset).split(':')

        # asteroid name
        if "asteroid designations" in ligne[0].lower():
            separate = ligne[1].split(',')
            try :
                target_name = separate[1].strip()
            except :
                target_name = separate[0].strip()

            param=Param(votable, name="target_name", datatype=jr["votable"]["target_name"]["datatype"], arraysize=jr["votable"]["target_name"]["arraysize"], ucd=jr["votable"]["target_name"]["ucd"], value=target_name)
            param.description=jr["votable"]["target_name"]["description"]
            table.params.append(param)

        #date 
        elif "(ut)" in ligne[0].lower():
            separate = ligne[1].split('-')
            annee = separate[0].strip()
            mois = separate[1].strip()
            jour = separate[2].strip()
            heure = separate[3].strip()+':'+ligne[2]
            thetime = annee + '-' + mois + '-' + jour + 'T' + heure
            param = Param(votable, name="date of observation", datatype=jr["votable"]["date"]["datatype"],arraysize=jr["votable"]["date"]["arraysize"], ucd=jr["votable"]["date"]["ucd"],value=thetime)
            param.description = jr["votable"]["target_name"]["description"]
            table.params.append(param)

        # creator
        elif "made by" in ligne[0].lower() :
            try :
                separate = ligne[1].split(',')
                creator = separate[0].replace("E-mail","").strip()
            except:
                creator=''
            param=Param(votable, name="DataID.Creator", datatype=jr["votable"]["creator"]["datatype"], arraysize=jr["votable"]["creator"]["arraysize"], ucd=jr["votable"]["creator"]["ucd"],  value=creator)
            param.description=jr["votable"]["creator"]["description"]
            table.params.append(param)


        # facility
        elif "uai code" in ligne[0].lower():
            try:
                facility = ligne[1].strip()
            except:
                facility = ''
            param=Param(votable, name="ObsConfig.Facility.Name", datatype=jr["votable"]["facility"]["datatype"],arraysize=jr["votable"]["facility"]["arraysize"], ucd=jr["votable"]["facility"]["ucd"], unit=jr["votable"]["facility"]["unit"],  value=facility)
            param.description=jr["votable"]["facility"]["description"]
            table.params.append(param)

        # reference
        elif "article" in ligne[0].lower():
            try :
                reference = ligne[1].strip()
            except:
                reference = ''
            param=Param(votable, name="Curation.Reference", datatype=jr["votable"]["reference"]["datatype"], arraysize=jr["votable"]["reference"]["arraysize"], ucd=jr["votable"]["reference"]["ucd"], value=reference)
            table.params.append(param)

        # reference
        elif "normalization" in ligne[0].lower():
            calibration = ligne[1].strip()
            param=Param(votable, name="Char.FluxAxis.CalibrationStatus", datatype=jr["votable"]["calibration"]["datatype"], arraysize=jr["votable"]["calibration"]["arraysize"], ucd=jr["votable"]["calibration"]["ucd"], value="NORMALIZED")
            param.description = calibration
            table.params.append(param)
            param=Param(votable, name="SpectralCalibration", datatype=jr["votable"]["calibration"]["datatype"], arraysize=jr["votable"]["calibration"]["arraysize"], ucd="meta.code.qual", utype="spec:Spectrum.Char.SpectralAxis.Accuracy.Calibration", value="NORMALIZED")
            table.params.append(param)

        # skip other lines in description
        elif "comments" in ligne[0].lower() or "old name" in ligne[0].lower() or "original name" in ligne[0].lower():
            pass
        # skip columns headers
        elif "wavelength[um]" in ligne[0].lower():
            pass
        # read data
        else:
            separate = ligne[0].split()
            #print(separate)
            wave.append(float(separate[0]))
            reflect.append(float(separate[1].replace("\\n'","")))
            if len(separate) >= 3:
                error.append(float(separate[2]))
                presence_error = 1

    #    print (wave, reflect, error)
    #   FIELD of spectrum
    field=Field(votable, name=jr["votable"]["wavelength"]["name"], datatype=jr["votable"]["wavelength"]["datatype"], ucd=jr["votable"]["wavelength"]["ucd"], unit=jr["votable"]["wavelength"]["unit"])
    table.fields.append(field)
    field = Field(votable, name=jr["votable"]["reflectance"]["name"], datatype=jr["votable"]["reflectance"]["datatype"],ucd=jr["votable"]["reflectance"]["ucd"], unit=jr["votable"]["reflectance"]["unit"])
    field.description = jr["votable"]["reflectance"]["description"]
    table.fields.append(field)
    if presence_error == 1:
        field = Field(votable, name=jr["votable"]["error"]["name"],datatype=jr["votable"]["error"]["datatype"], ucd=jr["votable"]["error"]["ucd"],unit=jr["votable"]["error"]["unit"])
        field.description = jr["votable"]["error"]["description"]
        table.fields.append(field)


    # define the table and its size
    table.create_arrays(len(wave))

# fill the votable : table
    for j in range(len(wave)):
        if presence_error == 1:
            table.array[j] = (wave[j],reflect[j],error[j])
        else:
            table.array[j] = (wave[j], reflect[j])



# REAL START OF THE PROGRAM:

# CASE WHEN YOU READ A CSV FILE WITH EPNCORE PARAM
# pick up one
i = 'm4ast.csv'
e = pd.read_csv(i)



for row in range(len(e)):  #all the csv 
    ligne = e.loc[row, :]

    # for each line, create a new VOTable file...
    votable = VOTableFile()
    # ...with one resource...
    resource = Resource()
    votable.resources.append(resource)
    # ... with one table
    table = Table(votable)
    resource.tables.append(table)
    resource._description="Spectrum converted from ascii version"
    # ... and call the transformation 
    toto = to_votable(ligne)
    if toto != None:
        print(toto)
    else:
        Nfichier = ligne["access_url"].split('/')[-1].split('.')[0]+'.vot'
        votable.to_xml(Nfichier)
    pl_thumb(ligne)
