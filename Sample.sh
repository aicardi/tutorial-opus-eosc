#!/bin/sh

# Fetches the list of spectra for the asteroid using ADQL
# Output is a file m4ast.csv
/usr/local/bin/tap_request.py ${asteroid} 

# For each line of the csv, fetches the ascii data,
# converts to a votable and plots a thumbnail
/usr/local/bin/make_votable_and_thumbnails.py
