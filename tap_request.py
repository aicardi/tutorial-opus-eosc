#!/usr/bin/python3

import pyvo
import sys

ASTEROID = sys.argv[1]
FILENAME = 'm4ast.csv'

m4ast_svc = pyvo.dal.TAPService("http://voparis-tap-planeto.obspm.fr/tap")
query = f"SELECT * FROM m4ast.epn_core WHERE ((dataproduct_type LIKE '%sp%') AND (target_class LIKE '%asteroid%') AND (access_format LIKE '%text%') AND (1 = ivo_hashlist_has(lower(target_name),lower('{ASTEROID}'))))"

# output function

def represent(value):
    if str(value)=='nan':
        return ''
    return str(value)

# fetch VOTable

vot = m4ast_svc.run_sync(query)
keys = vot.fieldnames

with open(FILENAME, 'w') as csv_file:
    csv_file.write(','.join(keys))
    csv_file.write('\r\n')
    for line in vot:
        csv_file.write(','.join([represent(line.get(key, decode=True)) for key in keys]))
        csv_file.write('\r\n')


