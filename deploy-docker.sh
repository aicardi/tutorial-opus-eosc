#!/bin/bash

# This script is meant to be launched on a new debian server with only git installed (to clone this repository obviously)

# Upgrade server to latest version
sudo apt update
sudo apt upgrade -y

# Install Docker
sudo apt install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
sudo curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
sudo echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list
sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io
sudo systemctl enable docker

# Patch and install OPUS
git clone https://github.com/ParisAstronomicalDataCentre/OPUS.git
sed -i 's/flask-security-too/flask-security-too<5.0.0/' OPUS/requirements.txt
cp -p Dockerfile OPUS/Dockerfile
cp -p settings_opus.py OPUS/settings_opus.py
cp -p apache_opus.conf OPUS/apache_opus.conf

# Sample job
#gcc sample.c -o OPUS/sample -lm
cp -p Sample.sh OPUS/Sample.sh
cp -p Sample_vot.xml OPUS/Sample_vot.xml
sudo docker build -t opus OPUS
sudo docker run -dt --name opus -p 80:80 opus

# Installation complete
IP=`ip addr show | grep "inet "| grep -v ' 127.0.0.1' | grep -v ' 172.17'| cut -f1 -d/ | cut -f6 -d\ `
echo 
echo "The installation is now complete"
echo "You can access the service here: http://$IP/opus_client/"
