#!/bin/bash

untar_private_infos () {
    if [ -s private_infos.tar.gz ] ; then
        tar xzf private_infos.tar.gz
    else
        echo "You must fetch private_infos.tar.gz from the master node first"
        exit 1
    fi
}

update_etc_hosts () {
    echo -e "$OPUS_IP $OPUS_NAME\n$SLURMCTL_IP $SLURMCTL_NODE" | sudo tee -a /etc/hosts
    for i in `seq 0 $SLURM_NODES`; do echo "${SLURM_IPS[$(($i-1))]} $SLURM_PREFIX$i" | sudo tee -a /etc/hosts; done
}

update_private_infos () {
    for i in `seq $SLURM_NODES`
    do
        ssh-keyscan -H $SLURM_PREFIX$i >> known_hosts
    done
    cp known_hosts $HOME/.ssh/known_hosts
    rm private_infos.tar.gz
    tar czf private_infos.tar.gz munge.key authorized_keys user_key slurm.conf known_hosts
}

predeploy_nodes () {
    for i in `seq $SLURM_NODES`
    do
        ssh $SLURM_PREFIX$i sudo apt update
        ssh $SLURM_PREFIX$i sudo apt install -y git
        ssh $SLURM_PREFIX$i "git clone https://gitlab.obspm.fr/aicardi/tutorial-opus-eosc.git"
        scp parameters.sh $SLURM_PREFIX$i:tutorial-opus-eosc/
        scp private_infos.tar.gz $SLURM_PREFIX$i:tutorial-opus-eosc/
    done
}

munge_first() {
    # install munge first to ensure its uid/gid
    sudo apt update
    sudo apt install -y libmunge-dev libmunge2 munge
}

common() {
    # common setup for node and master
    sudo cp ./munge.key /etc/munge/munge.key
    sudo chown munge:munge /etc/munge/munge.key
    sudo chmod 400 /etc/munge/munge.key
    sudo systemctl enable munge
    sudo systemctl restart munge
    sudo mv slurm.conf /etc/slurm/slurm.conf
    sudo groupadd -g $USER_UID $USER_SLURM
    sudo useradd -d /home/$USER_SLURM -u $USER_UID -g $USER_UID -m $USER_SLURM -s /bin/bash
    sudo mkdir /home/$USER_SLURM/.ssh -m 700
    sudo cp authorized_keys /home/$USER_SLURM/.ssh/
    sudo cp authorized_keys /home/$USER_SLURM/.ssh/id_rsa.pub
    sudo cp user_key /home/$USER_SLURM/.ssh/id_rsa
    sudo cp known_hosts /home/$USER_SLURM/.ssh/known_hosts
    sudo chown -R $USER_SLURM:$USER_SLURM /home/$USER_SLURM/.ssh
    sudo mkdir /home/$USER_SLURM/shared
    sudo chown $USER_SLURM:$USER_SLURM /home/$USER_SLURM/shared
    sudo apt install -y rsync sshfs
}

install_sample_code() {
    sudo apt install -y python3-pip
    sudo pip3 install -r Sample-requirements.txt
    sudo mkdir -p /usr/local/share/opus-sample/
    sudo cp -p m4ast.json /usr/local/share/opus-sample/
    sudo cp -p adql_request.sh /usr/local/bin/
    sudo cp -p tap_request.py /usr/local/bin/
    sudo cp -p make_votable_and_thumbnails.py /usr/local/bin/
}

install_elasticity_code() {
    sudo pip3 install fedcloudclient
    sudo mkdir -p /etc/grid-security/certificates
    CA_BUNDLE=https://dist.eugridpma.info/distribution/igtf/current/accredited/igtf-preinstalled-bundle-classic.tar.gz
    curl -s $CA_BUNDLE | sudo tar -xvz -C /etc/grid-security/certificates
    cat /etc/grid-security/certificates/*.pem | sudo tee -a $(python3 -m requests.certs)
    sudo mv elasticity/refresh_access_token /usr/local/sbin
    sudo mv elasticity/SlurmSuspendProgram /usr/local/sbin
    sudo mv elasticity/SlurmResumeProgram /usr/local/sbin
}

init_master() {
    munge_first
    untar_private_infos
    update_etc_hosts
    update_private_infos
    predeploy_nodes
    sudo apt install -y slurmctld slurmdbd mariadb-server
    zcat /usr/share/doc/slurmdbd/examples/slurmdbd.conf.simple.gz > slurmdbd.conf
    . slurmdbd.conf
    sudo mv slurmdbd.conf /etc/slurm/slurmdbd.conf
    sudo chown slurm /etc/slurm/slurmdbd.conf
    sudo chmod 600 /etc/slurm/slurmdbd.conf
    sudo mysql --execute "CREATE USER '$StorageUser'@'localhost' IDENTIFIED BY '$StoragePass';"
    sudo mysql --execute "GRANT ALL ON $StorageLoc.* TO '$StorageUser'@'localhost';"
    sudo systemctl enable slurmdbd
    sudo systemctl start slurmdbd
    sleep 2
    common
    sudo systemctl enable slurmctld
    sudo systemctl restart slurmctld
    sudo sacctmgr -i add account $USER_SLURM 
    sudo sacctmgr -i add user $USER_SLURM account=$USER_SLURM partition=opus
    sudo -u $USER_SLURM mkdir /home/$USER_SLURM/shared/jobdata/
    sudo -u $USER_SLURM mkdir /home/$USER_SLURM/shared/results/
    sudo -u $USER_SLURM mkdir /home/$USER_SLURM/shared/scripts/
    sudo -u $USER_SLURM mkdir /home/$USER_SLURM/shared/uploads/
    sudo -u $USER_SLURM mkdir /home/$USER_SLURM/shared/workdir/
    sudo cp Sample.sh /home/$USER_SLURM/shared/scripts/
    sudo chown $USER_SLURM /home/$USER_SLURM/shared/scripts/Sample.sh
    sudo chmod 755 /home/$USER_SLURM/shared/scripts/Sample.sh
    install_sample_code
    install_elasticity_code
    exit 0
}

init_node() {
    munge_first
    untar_private_infos
    sudo apt install -y slurmd sshfs
    sudo cp -p cgroup.conf /etc/slurm/
    update_etc_hosts
    common
    sudo systemctl enable slurmd
    sudo systemctl start slurmd
    echo "user_allow_other" | sudo tee -a /etc/fuse.conf
    echo -e "#!/bin/bash\n/usr/bin/sshfs $SLURMCTL_NODE:shared/ /home/$USER_SLURM/shared/ -o default_permissions -o allow_other" > mount_shared
    sudo mv mount_shared /usr/local/bin/mount_shared
    sudo chmod 755 /usr/local/bin/mount_shared
    sudo cp crontab /var/spool/cron/crontabs/$USER_SLURM
    sudo chown $USER_SLURM.crontab /var/spool/cron/crontabs/$USER_SLURM
    sudo chmod 600 /var/spool/cron/crontabs/$USER_SLURM
    sudo -u $USER_SLURM /usr/local/bin/mount_shared
    install_sample_code
    exit 0
}

usage () {
    echo "Usage: $0 [master]"
    echo "Prepare slurm configuration for node or for master"
    exit 1
}

# Check parameter

if [ $# -gt 1 ]; then
    usage
fi

if [ -s parameters.sh ]; then 
    . parameters.sh
else
    echo "You must copy parameters.sh.tmpl to parameters.sh and adapt its contents to your need"
    exit 1
fi

if [ $# -eq 1 ] && [ $1 == "master" ]; then
    init_master
fi

if [ $# -eq 1 ]; then
    usage
fi

init_node
