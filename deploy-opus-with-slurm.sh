#!/bin/bash

# This script is meant to be launched on a new debian server with only git installed (to clone this repository obviously)

# Upgrade server to latest version
sudo apt update
sudo apt upgrade -y

# Install necessary dependencies
sudo apt install -y --no-install-recommends apache2 apache2-dev apache2-utils vim locales graphviz
sudo apt install -y python3 python3-pip libpq-dev

# Read parameters.sh
. parameters.sh
SLURMCTL_NET=`echo $SLURMCTL_IP | cut -d. -f1-3`

# Patch and install OPUS
git clone https://github.com/ParisAstronomicalDataCentre/OPUS.git
sed -i 's/flask-security-too/flask-security-too<5.0.0/' OPUS/requirements.txt
sed -i "s,^\(\s*self.ssh_arg_uws.*\)$,\1.split('/')[0]," OPUS/uws_server/managers.py

echo -e "$OPUS_IP $OPUS_NAME\n$SLURMCTL_IP $SLURMCTL_NODE ${SLURM_PREFIX}0" | sudo tee -a /etc/hosts
for i in `seq $SLURM_NODES`; do echo "${SLURM_IPS[$(($i-1))]} $SLURM_PREFIX$i" | sudo tee -a /etc/hosts; done
sudo bash <<EOF
pip install -r OPUS/requirements.txt
sed -i 's/bundle.label ==/bundle.identifier !=/g' /usr/local/lib/python3.9/dist-packages/prov/dot.py
sed -i 's/six.text_type(bundle.label)/six.text_type(bundle.identifier)/g' /usr/local/lib/python3.9/dist-packages/prov/dot.py
pip install mod-wsgi
rm /etc/apache2/sites-enabled/000-default.conf

mkdir /opt/opus
cp -p OPUS/favicon* /opt/opus/
cp -pr OPUS/uws_client /opt/opus/uws_client
cp -pr OPUS/uws_server /opt/opus/uws_server
cp -p OPUS/uws_client/wsgi.py /opt/opus/wsgi_client.py
cp -p OPUS/uws_server/wsgi.py /opt/opus/wsgi_server.py
sed -e "s/OPUS_IP/$OPUS_IP/;s/SLURMCTL_IP/$SLURMCTL_IP/;s/SLURMCTL_NET/$SLURMCTL_NET/;s/OPUS_USERNAME/$USER_SLURM/;s/SLURM_NODES/$SLURM_NODES/" settings_opus_slurm.py > /opt/opus/settings_local.py

rm /etc/apache2/sites-enabled/000-default.conf
mod_wsgi-express module-config > /etc/apache2/mods-enabled/wsgi.conf
cp -p apache_opus.conf /etc/apache2/sites-enabled/apache_opus.conf
# test if OPUS_NAME is the reverse of OPUS_IP
if [ $OPUS_NAME == `host $OPUS_IP | cut -f5 -d\ | sed -e 's/.$//'` ]; then
  sed -e "s/OPUS_NAME/$OPUS_NAME/;s,OPUS_PUBLIC_CERT_FILE,$OPUS_PUBLIC_CERT_FILE,;s,OPUS_PRIVATE_KEY_FILE,$OPUS_PRIVATE_KEY_FILE,;s,OPUS_CERT_CHAIN,$OPUS_CERT_CHAIN," apache_opus_https.conf.tmpl >/etc/apache2/sites-enabled/apache_opus_https.conf
  a2enmod ssl
fi

mkdir -p /var/www/opus/logs /var/www/opus/jdl/scripts /var/www/opus/jdl/votable
chown -R www-data /var/www/opus
EOF

# Private infos
dd if=/dev/urandom bs=1 count=1024 > munge.key
ssh-keygen -N "" -f ./user_key
cp -p user_key.pub authorized_keys
sed -e "s/HOST_NAME/${SLURMCTL_NODE}(${SLURMCTL_INT_IP})/;s/NB_NODES/${SLURM_NODES}/;s/NODE_NAME/${SLURM_PREFIX}/" slurm.conf.tmpl > slurm.conf
ssh-keyscan -H $OPUS_IP > known_hosts
ssh-keyscan -H $SLURMCTL_IP >> known_hosts
ssh-keyscan -H $SLURMCTL_NODE >> known_hosts
ssh-keyscan -H ${SLURM_PREFIX}0 >> known_hosts
tar czf private_infos.tar.gz munge.key authorized_keys user_key slurm.conf known_hosts
sudo mkdir /var/www/.ssh
sudo cp authorized_keys /var/www/.ssh/authorized_keys
sudo cp user_key /var/www/.ssh/id_rsa
sudo cp known_hosts /var/www/.ssh/known_hosts
sudo chown -R www-data:www-data /var/www/.ssh
sudo chmod 700 /var/www/.ssh
mkdir -p ~/.ssh
chmod 700 ~/.ssh
cp known_hosts ~/.ssh/known_hosts
sudo chsh -s /bin/sh www-data

# For the sample job
sudo pip install -r Sample-requirements.txt
sudo cp -p Sample.sh /var/www/opus/jdl/scripts/
sudo cp -p Sample_vot.xml /var/www/opus/jdl/votable/

sudo systemctl reload apache2

# Prepare slurm nodes
ssh $ADM_USER@$SLURMCTL_NODE sudo apt update
ssh $ADM_USER@$SLURMCTL_NODE sudo apt install -y git
ssh $ADM_USER@$SLURMCTL_NODE "git clone https://gitlab.obspm.fr/aicardi/tutorial-opus-eosc.git"
scp parameters.sh $ADM_USER@$SLURMCTL_NODE:tutorial-opus-eosc/
scp private_infos.tar.gz $ADM_USER@$SLURMCTL_NODE:tutorial-opus-eosc/

# Installation complete
IP=`ip addr show | grep "inet "| grep -v ' 127.0.0.1' | cut -f1 -d/ | cut -f6 -d\ `
echo 
echo "The installation is now complete"
echo "You can access the service here: http://$IP/opus_client/"
