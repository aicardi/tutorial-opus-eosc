#!/bin/bash

# This script is meant to be launched on a new debian server with only git installed (to clone this repository obviously)

# Upgrade server to latest version
sudo apt update
sudo apt upgrade -y

# Install necessary dependencies
sudo apt install -y --no-install-recommends apache2 apache2-dev apache2-utils vim locales graphviz
sudo apt install -y python3 python3-pip libpq-dev

# Patch and install OPUS
git clone https://github.com/ParisAstronomicalDataCentre/OPUS.git
sed -i 's/flask-security-too/flask-security-too<5.0.0/' OPUS/requirements.txt

sudo bash <<EOF
pip install -r OPUS/requirements.txt
sed -i 's/bundle.label ==/bundle.identifier !=/g' /usr/local/lib/python3.9/dist-packages/prov/dot.py
sed -i 's/six.text_type(bundle.label)/six.text_type(bundle.identifier)/g' /usr/local/lib/python3.9/dist-packages/prov/dot.py
pip install mod-wsgi
rm /etc/apache2/sites-enabled/000-default.conf

mkdir /opt/opus
cp -p OPUS/favicon* /opt/opus/
cp -pr OPUS/uws_client /opt/opus/uws_client
cp -pr OPUS/uws_server /opt/opus/uws_server
cp -p OPUS/uws_client/wsgi.py /opt/opus/wsgi_client.py
cp -p OPUS/uws_server/wsgi.py /opt/opus/wsgi_server.py
cp -p settings_opus.py /opt/opus/settings_local.py

rm /etc/apache2/sites-enabled/000-default.conf
mod_wsgi-express module-config > /etc/apache2/mods-enabled/wsgi.conf
cp -p apache_opus.conf /etc/apache2/sites-enabled/apache_opus.conf
mkdir -p /var/www/opus/logs /var/www/opus/jdl/scripts /var/www/opus/jdl/votable
chown -R www-data /var/www/opus
EOF

# For the sample job
sudo pip install -r Sample-requirements.txt
sudo mkdir -p /usr/local/share/opus-sample/
sudo cp -p m4ast.json /usr/local/share/opus-sample/
sudo cp -p adql_request.sh /usr/local/bin/
sudo cp -p make_votable_and_thumbnails.py /usr/local/bin/
sudo cp -p Sample.sh /var/www/opus/jdl/scripts/
sudo cp -p Sample_vot.xml /var/www/opus/jdl/votable/

sudo systemctl reload apache2

# Installation complete
IP=`ip addr show | grep "inet "| grep -v ' 127.0.0.1' | cut -f1 -d/ | cut -f6 -d\ `
echo 
echo "The installation is now complete"
echo "You can access the service here: http://$IP/opus_client/"
