# OPUS global config

BASE_URL = 'http://OPUS_IP/opus_server'
BASE_IP = 'OPUS_IP'
ADMIN_EMAIL = ''
ADMIN_NAME = 'opus-admin'
MAIL_SERVER = 'smtp.'
MAIL_PORT = 25
SENDER_EMAIL = ''  # e.g. no_reply@example.com
# Directory where app data is stored - has to be writable for the web server (www, _www, apache...)
VAR_PATH = '/var/www/opus'

# Server global config

# IMPORTANT: use random strings for the following tokens and keep them secret
ADMIN_TOKEN = '14a5e625-1720-5f34-9c28-c330b87d9f3c'  # TOKEN of admin user
JOB_EVENT_TOKEN = '14a5e625-1720-5f34-9c28-c330b87d9f3c'  # TOKEN for special user job_event, used internally
MAINTENANCE_TOKEN = '14a5e625-1720-5f34-9c28-c330b87d9f3c'  # TOKEN for special user maintenance, used internally
# Access rules
ALLOW_ANONYMOUS = True
CHECK_PERMISSIONS = False  # check rights to run/edit a job
CHECK_OWNER = False  # only owner can access their files
NJOBS_MAX = SLURM_NODES
# job servers can have access to /job_event/<jobid_manager> to change the phase or report an error
# The IP can be truncated to allow to refer to a set of IPs (e.g. '127.' for 127.*.*.*)
JOB_SERVERS = {
    '::1': 'localhost',
    '127.0.0.1': 'localhost',
    'SLURMCTL_NET': 'my_cluster',
}
# trusted clients can have access to /db and /jdl (while waiting for an A&A system)
# e.g. /db/init, /jdl/validate...
TRUSTED_CLIENTS = {
    '::1':       'localhost',
    '127.0.0.1': 'localhost',
    BASE_IP: 'base_ip',
}

# Client global config

# IMPORTANT: keep those passwords secret
ADMIN_DEFAULT_PW = 'opus-admin'
TESTUSER_DEFAULT_PW = 'testuser'

CLIENT_TITLE = "OPUS"
HOME_CONTENT = "<h3>OPUS</h3><p>Observatoire de Paris UWS Server - http://opus-job-manager.readthedocs.io</p>"

# Database

STORAGE_TYPE = 'SQLite'

# More config (see uws_server/settings.py and uws_client/settings.py)
OIDC_IDPS = []

MANAGER = 'SLURM'
SLURM_URL = 'SLURMCTL_IP'
SLURM_USER = 'OPUS_USERNAME'
SLURM_MAIL_USER = ADMIN_EMAIL
SLURM_SCRIPTS_PATH = '/home/OPUS_USERNAME/shared/scripts'
SLURM_JOBDATA_PATH = '/home/OPUS_USERNAME/shared/jobdata'
SLURM_UPLOADS_PATH = '/home/OPUS_USERNAME/shared/uploads'
SLURM_WORKDIR_PATH = '/home/OPUS_USERNAME/shared/workdir'
SLURM_RESULTS_PATH = '/home/OPUS_USERNAME/shared/results'
SLURM_SBATCH_DEFAULT = {
    'mem': '200mb',
    'nodes': 1,
    'ntasks-per-node': 1,
    'partition': 'opus',
    'account': 'opususer',
}
LOCAL_USER = 'www-data'
