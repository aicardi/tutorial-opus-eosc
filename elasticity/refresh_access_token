#!/usr/bin/python3

from urllib import request, parse
import json
import sys

AUTH_PORTAL = 'https://aai.egi.eu/auth/realms/egi/'
REFRESH_URI = 'protocol/openid-connect/token'
CHECK_URI = 'protocol/openid-connect/userinfo'
REFRESH_TOKEN_FILENAME = '/usr/local/etc/refresh_token'
ACCESS_TOKEN_FILENAME = '/usr/local/etc/access_token'

def read_refresh_token():
    FILENAME = 'refresh_token'
    with open(REFRESH_TOKEN_FILENAME) as token:
        return token.read()[:-1]

def get_access_token(refresh_token):
    data = {'grant_type': 'refresh_token',
            'refresh_token': refresh_token,
            'client_id': 'token-portal',
            'scope': 'openid email profile voperson_id eduperson_entitlement'
            }

    payload = parse.urlencode(data, quote_via=parse.quote).encode()
    req = request.Request(AUTH_PORTAL+REFRESH_URI,
                          data=payload,
                          method='POST')
    with request.urlopen(req) as resp:
        result = json.load(resp)
        if 'access_token' in result:
            with open(ACCESS_TOKEN_FILENAME, 'w') as access:
                access.write(result['access_token'])
                return result['access_token']

def check_access_token(access_token):
    auth_token = {'Authorization': f'Bearer {access_token}'}
    req = request.Request(AUTH_PORTAL+CHECK_URI, headers=auth_token)
    try:
        with request.urlopen(req) as resp:
            result = json.load(resp)
            if 'error' in result:
                print(f'Error: result["error_description"]')
                return False
            return True
    except: 
        return False

try:
    with open(ACCESS_TOKEN_FILENAME) as access:
        access_token = access.read()[:-1]
except FileNotFoundError:
    access_token = ''

if not check_access_token(access_token):
    access_token = get_access_token(read_refresh_token())
    if check_access_token(access_token):
        sys.exit(0)
    sys.exit(1)
sys.exit(0)


